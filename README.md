# delegate-client

## Project setup

```shell
npm install
```

### Compiles and hot-reloads for development

```shell
npm run serve
```

### Compiles and minifies for production

```shell
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Resources for development

#### Authentication template

[Flask](https://fareedidris.medium.com/cookie-based-authentication-using-flask-and-vue-js-part-1-c625a530c157)

[Vue](https://fareedidris.medium.com/cookie-based-authentication-using-flask-and-vue-js-part-2-bd2b47545466)

[Good one](https://stackabuse.com/single-page-apps-with-vue-js-and-flask-jwt-authentication/s)
