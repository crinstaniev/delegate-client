import axios from 'axios';
import { API_URL } from '../config';

export const login = async (email, password) => {
  const res = await axios
    .post(`${API_URL}/auth/login`, {
      email: email,
      password: password
    })
    .catch(err => {
      console.log('[ERR] login failed', {
        status: err.response.status,
        data: err.response.data
      });
      return false;
    });

  return res;
};

export const signup = async ({ name, email, password, signature }) => {
  const res = await axios
    .post(`${API_URL}/user`, {
      name,
      email,
      password,
      signature
    })
    .catch(err => {
      console.log('[ERR] registration failed', {
        status: err.response.status,
        data: err.response.data
      });

      return false;
    });

  return res;
};
