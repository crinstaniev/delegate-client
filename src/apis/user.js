import axios from 'axios';
import { API_URL } from '../config';

export const getCurrentUser = async token => {
  const res = await axios
    .get(`${API_URL}/user/current`, {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    .catch(err => {
      console.log('[ERR] registration failed', {
        status: err.response.status,
        data: err.response.data
      });

      return false;
    });

  return res['data'];
};
