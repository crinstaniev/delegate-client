import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/dashboard'
  },
  {
    path: '/dashboard',
    name: 'Home',
    component: () => import('../views/Dashboard')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login')
  },
  {
    path: '/signup',
    name: 'Sign Up',
    component: () => import('../views/SignUp')
  },
  {
    path: '/signup/success',
    name: 'Success',
    component: () => import('../views/SignUpSuccess')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
